#(c) Copyright 2013 Emrah Kostem. All Rights Reserved. 

import os
import sys
import numpy as np


def parseGWFile( heidiGWFilename ):
    fp = open( heidiGWFilename, "r")
    fp.readline() ## skip header
    line = fp.readline()
    return float( line.strip().split()[1] )



def parseRegionFile( heidiRegionFilename ):
    fp = open( heidiRegionFilename, "r" )
    fp.readline() ## skip header
    lines = fp.readlines()
    vals = {}
    for line in lines:
        line = line.strip().split()
        vals[ line[0] ] = float( line[1] )
    return vals["Region:"]

    




if __name__ == '__main__':
    
    chroms = [ str(i) for i in range(1,23) ]
    #chroms.append( 'X' )
    
    heidiPrefix = sys.argv[1]
    
    gwHeritability = parseGWFile( heidiPrefix + ".GW" )
    
    chromHeritabilities = np.zeros( len(chroms) )
    i = -1
    for chrom in chroms:
        i += 1
        chromFilename = """%s.chr%s""" % ( heidiPrefix, chrom )
        if ( not os.path.exists( chromFilename ) ):
            print """Chrom: %s heritability file doesn't exist!""" % chrom
            sys.exit()
        chromHeritabilities[ i ] = parseRegionFile( chromFilename )
    
    chromScalar = gwHeritability / np.sum( chromHeritabilities )
    chromHeritabilities *= chromScalar

    ## We assume regions are consecutive and index starts from 1
    chromPartitions = {}
    i = -1
    for chrom in chroms:
        i += 1
        regionId = 1
        regionHeritabilities = []
        while( True ):
            regionFilename = """%s.chr%s.region%s""" % ( heidiPrefix, chrom, regionId )
            if ( not os.path.exists( regionFilename ) ):
                if ( regionId == 1 ):
                    print """Chrom: %s should have at least one region!""" % chrom
                    sys.exit()
                else:
                    break
            regionHeritabilities.append( parseRegionFile( regionFilename ) )
            regionId += 1
        regionHeritabilities = np.array( regionHeritabilities )
        scalar = chromHeritabilities[ i ] / np.sum( regionHeritabilities )
        chromPartitions[ chrom ] = scalar*regionHeritabilities
        


    outFilename = heidiPrefix + ".normChrom.txt"
    fp = open( outFilename, "w" )
    fp.write( " ".join( [str(i) for i in chromHeritabilities] ) + "\n" )
    fp.close()

    outFilename = heidiPrefix + ".normRegions.txt"
    fp = open( outFilename, "w" )
    for chrom in chroms:
        fp.write( " ".join( [ str(i) for i in chromPartitions[ chrom ] ] ) + "\n" )
    fp.close()


