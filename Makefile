OS = $(shell uname -s)
CXX = icpc -std=c++11 
CFLAGS = -O3 -DNDEBUG -Wall

LIBDIR = -L$(MKLROOT)/lib/
INCLUDE = -I$(MKLROOT)/include

LIBDIR += -L$(BOOSTROOT)/lib/
INCLUDE += -I$(BOOSTROOT)/include

LIBDIR += -L$(ARMADILLOROOT)/lib/
INCLUDE += -I$(ARMADILLOROOT)/include

CFLAGS += -static-intel

ifeq ($(OS),Darwin)
	CFLAGS += -gcc-name=/opt/local/bin/gcc-mp-4.6
	LIBS = -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lpthread
endif

ifeq ($(OS), Linux)
	CFLAGS += -static
	LIBS = -Wl,--start-group -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -Wl,--end-group -lpthread
endif
LIBS += -lsqlite3
LIBS += -lboost_program_options


OBJECTS = readPLINK.o heidi.o main.o


all: $(OBJECTS)
	$(CXX) $(CFLAGS) $(OBJECTS) $(INCLUDE) $(LIBDIR) $(LIBS) -o heidi
	ctags -R --sort=yes --c++-kinds=+p --fields=+iaS --extra=+q .

readPLINK.o: ./src/readPLINK.cpp
	$(CXX) $(CFLAGS) -c ./src/readPLINK.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

heidi.o: ./src/heidi.cpp
	$(CXX) $(CFLAGS) -c ./src/heidi.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

main.o: ./src/main.cpp
	$(CXX) $(CFLAGS) -c ./src/main.cpp $(INCLUDE) $(LIBDIR) $(LIBS)



clean:
	rm *.o


