
#include <iostream>
#include <fstream>

using namespace std;

#include <boost/program_options.hpp>
namespace po = boost::program_options;


#include "../include/readPLINK.h"
#include "../include/heidi.h"

void printHeader()
{
    cout << endl;
    cout << "@---------------------------------------------------@" << endl;
    cout << "|     HEIDI     |     v0.1     |     03/14/2013     |" << endl;
    cout << "|---------------------------------------------------|" << endl;
    cout << "|   © 2013 Emrah Kostem, Academic Public License    |" << endl;
    cout << "|---------------------------------------------------|" << endl;
    cout << "|   For documentation visit:                        |" << endl;
    cout << "|   http://genetics.cs.ucla.edu/heritability        |" << endl;
    cout << "@---------------------------------------------------@" << endl;
    cout << endl;
}

bool checkFileExists( string filename )
{
    ifstream fp( filename.c_str() );
    bool ret = false;
    if (fp.good())
    {
        ret = true;
    }
    fp.close();
    return ret;
}

int main( int ac, char* av[] )
{
    
    string logFilename, bedPrefix, covarFilename, phenoFilename, outPrefix, outputFilename;
    string fromSNP, toSNP, chrom;
    int outID = 0;
    int mpheno;
    double fromKB, toKB;

    string RRMgwFilename = "";
    string RRMrFilename = "";

    int maxMemoryGb = 2;

    logFilename = "./heidi.log";
    bedPrefix = "";
    phenoFilename = "";
    covarFilename = "";

    fromSNP = "";
    toSNP = "";
    
    chrom = "";
    fromKB = 0;
    toKB = 0;

    bool isRegionEntered = false;
    bool useWholeChoromosome = false;
    bool useWholeGenome = false;
    bool isRRMwrite = false;
    
    int numSteps = 20;
    mpheno = 0;
    
    printHeader();

    try
    {
        po::options_description generic("Allowed options");
        generic.add_options()
            ("help,h", "help\n")

            ("bfile",         po::value<string>(&bedPrefix),       "prefix for BIM/BED/FAM PLINK files")
            ("pheno",         po::value<string>(&phenoFilename),   "file for phenotypes")
            ("mpheno",        po::value<int>(&mpheno),             "which phenotype column to use (>=1, default:1)")
            ("covar",         po::value<string>(&covarFilename),   "file for covariates")
            ("out",           po::value<string>(&outPrefix),       "prefix for output file")
            ("id",            po::value<int>(&outID),              "region id\n")

            
            ("chr",           po::value<string>(&chrom),           "chromosome ('genomewide' estimates the total heritability)\n")

            ("from-kb",       po::value<double>(&fromKB),          "select region from this position (kb)")
            ("to-kb",         po::value<double>(&toKB),            "up to this position (kb)\n")

            ("from",          po::value<string>(&fromSNP),         "select region from this SNP (rsid)")
            ("to",            po::value<string>(&toSNP),           "to this SNP (must be on the same chromosome)\n")

            ("rrm-genome",    po::value<string>(&RRMgwFilename),   "Realized Relationship Matrix for the whole-genome\n")
            //("rrm-region",    po::value<string>(&RRMrFilename),    "Realized Relationship Matrix for the region")
            //("write-rrm",                                           "save RRM matrices\n")

            ("nsteps",        po::value<int>(&numSteps),           "number of iterations in grid search (default:20)")
            ("log",           po::value<string>(&logFilename),     "log file")
            ("maxmem",        po::value<int>(&maxMemoryGb),        "maximum Gb RAM usage allowed (default: 2)")
            
        ;

        po::variables_map vm;
        po::store( po::parse_command_line( ac, av, generic), vm);
        po::notify(vm);    
    
        if ( ac < 3 || vm.count("help") )
        {
            cout << generic << endl;
            return 1;
        }

        if ( !vm.count( "bfile" ) )
        {
            cerr << "(--bfile): Please specify PLINK files" << endl;
            

            abort();
        }
        else
        {
            
            if ( !checkFileExists( bedPrefix + ".bim" ) || !checkFileExists(bedPrefix + ".fam" ) || !checkFileExists(bedPrefix + ".bed" ) )
            {
                cerr << "(--bfile): Cannot open PLINK files" << endl;
                abort();
            }


            if ( !vm.count( "pheno" ) )
            {
                phenoFilename = bedPrefix + ".pheno";
                if ( !checkFileExists(phenoFilename) )
                {
                    cerr << "(--pheno): Cannot open phenotype file" << endl;
                    abort();
                }
            }
    
            if ( !vm.count( "covar" ) )
            {
                covarFilename = bedPrefix + ".covar";
                if ( !checkFileExists(covarFilename) )
                {
                    cerr << "(--covar): Cannot open covariates file" << endl;
                    abort();
                }
            }
        }

        if ( !vm.count( "out" ) )
        {
            cerr << "(--out): Please specify output file" << endl;
            abort();
        }
    
        if ( vm.count( "mpheno" ) )
        {
            if ( mpheno <=0 )
            {
                cerr << "(--mpheno): Phenotype index starts from 1" << endl;
                abort();
            }
            mpheno--;
        }
        
        if ( !vm.count( "chr" ) )
        {
            cerr << "(--chr): Please specify a chromosome from the BIM file or use 'genomewide' for total heritability" << endl;
            abort();
        }
        else
        {
            isRegionEntered = true;
            if ( chrom == "genomewide" )
            {
                useWholeGenome = true;
            }
            else if ( !vm.count( "from-kb" ) && !vm.count( "to-kb" ) && !vm.count( "from" ) && !vm.count ("to" ) )
            {
                useWholeChoromosome = true;
                if ( vm.count( "id" ) )
                {
                    cerr << "(--id): Please specify an id only for regions" << endl;
                }
            }
            else if ( vm.count( "from-kb" ) && vm.count( "to-kb" ) &&  !vm.count( "from" ) && !vm.count ("to" ) )
            {
                               
                if ( fromKB < 0 )
                {
                    cerr << "(--from-kb): Invalid start position for region" << endl;
                    abort();
                }

                if ( toKB <= 0 )
                {
                    cerr << "(--to-kb): Invalid ending position for region" << endl;
                    abort();
                }
            }
            else if ( vm.count( "from-kb" ) && !vm.count( "to-kb" ) )
            {
                cerr << "(--from-kb): Missing (--to-kb)" << endl;
                abort();
            }
            else if ( !vm.count( "from-kb" ) && vm.count( "to-kb" ) )
            {
                cerr << "(--to-kb): Missing (--from-kb)" << endl;
                abort();
            }
            else if ( vm.count( "from" ) && !vm.count( "to" ) )
            {
                cerr << "(--from): Missing (--to)" << endl;
                abort();
            }
            else if ( !vm.count( "from" ) && vm.count( "to" ) )
            {
                cerr << "(--to): Missing (--from)" << endl;
                abort();
            }
        }

        if ( !useWholeChoromosome && !useWholeGenome )
        {
            if ( !vm.count( "id" ) )
            {
                cerr << "(--id): Please specify an id for region" << endl;
                abort();
            }
            else
            {
                if ( outID <= 0 )
                {
                    cerr << "(--id): Invalid id" << endl;
                    abort();
                }
            }
        }

        if ( vm.count( "write-rrm" ) )
        {
            if ( !vm.count( "rrm-region" ) || !vm.count("rrm-genome") )
            {
                cerr << "(--write-rrm): Please specify Realized Relationship Matrix file names" << endl;
                abort();
            }
            isRRMwrite = true;
        }


        if ( vm.count( "rrm-genome" ) && !vm.count( "write-rrm") )
        {
            if ( !checkFileExists(RRMgwFilename) )
            {
                cerr << "(--rrm-genome): Cannot open RRM file for the whole genome" << endl;
                abort();
            }
        }

        if ( vm.count( "rrm-region" ) && !vm.count( "write-rrm") )
        {
            if ( !checkFileExists(RRMrFilename) )
            {
                cerr << "(--rrm-region): Cannot open RRM file of the region" << endl;
                abort();
            }
        }

        if ( vm.count( "nsteps" ) )
        {
            if ( (numSteps < 5) || (numSteps > 500 ) )
            {
                cerr << "(--nsteps): Infeasible number of steps, setting to 20" << endl;
                numSteps = 20;
            }
        }

        if ( useWholeChoromosome )
        {
            outputFilename = outPrefix + ".chr" + chrom;
        }
        else if ( useWholeGenome )
        {
            outputFilename = outPrefix + ".GW";
        }
        else
        {
            outputFilename = outPrefix + ".chr" + chrom + ".region" + to_string( outID );
        }
        
        if ( !vm.count("log") )
        {
            logFilename = outputFilename + ".log";
        }

    }
    catch( std::exception &e )
    {
        cerr << e.what() << endl;
    }
    


    cout << "Running..." << endl;
    cout << "For details see: " << logFilename << endl;

    ofstream logFILE( logFilename.c_str() ); 
    logFILE.precision(10);

    logFILE << "Running HEIDI:" << endl;
    logFILE << "Output File: " << outputFilename << endl;
    
    ofstream outFILE( outputFilename.c_str() );
    if ( !outFILE.is_open() )
    {
        cerr << "Cannot open output file for writing" << endl;
        logFILE << "Cannot open output file for writing" << endl;
        abort();
    }


    logFILE << "Using phenotype number: " << mpheno << endl;
    logFILE << "Chromosome: " << chrom << endl;
    
    readPLINKBED BED( bedPrefix, &logFILE );
    
    if ( !BED.createTableGENERIC( "phenoTable", phenoFilename ) )
    {
        cerr << "Cannot read phenotype file" << endl;
        abort();
    }

    if ( !BED.createTableGENERIC( "covarTable", covarFilename ) )
    {
        cerr << "Cannot read covariate file" << endl;
        abort();
    }
    
    
    logFILE << "#steps in grid search: " << numSteps << endl;


    HEIDI *H = NULL;
    
    if ( isRegionEntered )
    {
        if ( fromSNP.size() && toSNP.size() )
        {
            H = new HEIDI( chrom, fromSNP, toSNP, &BED, &logFILE, maxMemoryGb, RRMgwFilename, RRMrFilename, isRRMwrite );
        }
        else if ( fromKB >= 0 && toKB > 0 )
        {
            H = new HEIDI( chrom, fromKB, toKB, &BED, &logFILE, maxMemoryGb, RRMgwFilename, RRMrFilename, isRRMwrite );
        }
        else
        {
            H = new HEIDI( chrom, &BED, &logFILE, maxMemoryGb, RRMgwFilename, RRMrFilename, isRRMwrite );
        }
    }
    else
    {
        cerr << "Shouldn't be there :)" << endl;
        abort();
    }

    outFILE.precision(10);
    if ( !useWholeGenome )
    {
        H->setNumGoldenSteps(numSteps);
        H->setw(0.0);
        H->maxREML_w();
        double w = H->getw();
        double h2 = H->geth2();
        double reml = H->getREML(h2);
        
        logFILE << "w:\t" << w << endl;
        logFILE << "h2:\t" << h2 << endl;
        logFILE << "REML:\t" << reml << endl;

        outFILE << "HEIDI unnormalized heritabilities\n";
        outFILE << "Region:\t" << w*h2 << endl;
        outFILE << "Background:\t" << (1.0 - w)*h2 << endl;
        outFILE << "Total:\t" << h2 << endl;
    }
    else
    {
        H->setw( 0.0 );
        double h2 = H->maxREML_h2();
        double reml = H->getREML(h2);
        logFILE << "h2:\t" << h2 << endl;
        logFILE << "REML:\t" << reml << endl;

        outFILE << "HEIDI genome-wide heritability\n";
        outFILE << "Total:\t" << h2 << endl;
    }
    // CLEAN-UP
    if ( H ) delete H;
    logFILE.close();
    outFILE.close();
    return 0;
}

