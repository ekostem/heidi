#include "../include/heidi.h"


HEIDI::HEIDI( string chrom, string fromSNP, string toSNP, readPLINKBED* PL, ofstream* logFILE, int maxMemoryGb, string RRMgwFilename, string RRMrFilename, bool isRRMwrite )
{   
    m_PL = PL;
    m_logFILE = logFILE;
    
    if ( m_logFILE )
    {
        *m_logFILE << "HEIDI: Working on chromosome: " << chrom << " on region from SNP " << fromSNP << " to " << toSNP << endl;
    }


    unsigned long startIndex, stopIndex;

    PL->getSNPIndex(chrom, fromSNP, startIndex);
    PL->getSNPIndex(chrom, toSNP, stopIndex);
    Init( startIndex, stopIndex, maxMemoryGb, RRMgwFilename, RRMrFilename, isRRMwrite );
}


HEIDI::HEIDI( string chrom, double fromKB, double toKB, readPLINKBED* PL, ofstream* logFILE, int maxMemoryGb, string RRMgwFilename, string RRMrFilename, bool isRRMwrite )
{
    
    m_PL = PL;
    m_logFILE = logFILE;
    
    if ( m_logFILE )
    {
        *m_logFILE << "HEIDI: Working on chromosome: " << chrom << " on region from" << fromKB << " to " << toKB << endl;
    }



    unsigned long startIndex, stopIndex;
    PL->getSNPIndex(chrom, (unsigned long)(fromKB*1000.0), startIndex);
    PL->getSNPIndex(chrom, (unsigned long)(toKB*1000.0), stopIndex);
    Init( startIndex, stopIndex, maxMemoryGb, RRMgwFilename, RRMrFilename, isRRMwrite );
}

HEIDI::HEIDI( string chrom, readPLINKBED* PL, ofstream* logFILE, int maxMemoryGb, string RRMgwFilename, string RRMrFilename, bool isRRMwrite )
{
    m_PL = PL;
    m_logFILE = logFILE;
    unsigned long startIndex, stopIndex;
    
    startIndex = 0;
    stopIndex = 0;
    bool wholeGenome = false;

    if ( m_logFILE )
    {
        if ( chrom == "genomewide" )
        {
           if ( m_logFILE ) *m_logFILE << "HEIDI: Working on whole-genome" << endl;
            wholeGenome = true;
        }
        else
        {
            if ( RRMrFilename.size() )
            {
               if ( m_logFILE ) *m_logFILE << "HEIDI: Working on a region with RRM: " << RRMrFilename << endl;
            }
            else
            {
                if ( m_logFILE ) *m_logFILE << "HEIDI: Working on whole chromosome: " << chrom << endl;
                PL->getChromSNPIndices(chrom, startIndex, stopIndex);
            }
        }
    }
    Init( startIndex, stopIndex, maxMemoryGb, RRMgwFilename, RRMrFilename, isRRMwrite, wholeGenome );
}


void HEIDI::Init( unsigned long regionStartSNPIndex, unsigned long regionStopSNPIndex, int maxMemoryGb, string RRMgwFilename, string RRMrFilename, bool isRRMwrite, bool wholeGenome )
{ 
    m_w = 0.0;
    m_h2 = -1.0;
    m_wTop = 0.2;
    m_numSNPs = 0;
    m_numRegionSNPs = 0;
    m_numBackgroundSNPs = 0;
    m_numGoldenSteps = 20;

    bool useWholeGenome = wholeGenome;

    if ( m_logFILE ) *m_logFILE << "Initializing HEIDI" << endl;

    m_numInds = m_PL->getNumIndividuals();
    m_numSNPs = m_PL->getNumSNPs();

    /**********PHENOTYPE & COVARIATES ********************/

    if ( m_logFILE ) *m_logFILE << "HEIDI: Reading Phenotypes" << endl;
    if ( !m_PL->getTableMat( "phenoTable", mat_Y ) )
    {
        if ( m_logFILE ) *m_logFILE << "Error: HEIDI reading phenotypes" << endl;
        abort();
    }
    *m_logFILE << "#phenos: " << mat_Y.n_cols << endl;
    // By default set the first phenotype to work with
    mat_y = mat_Y.col(0);

    if ( m_logFILE ) *m_logFILE << "HEIDI: Reading Covariates" << endl;
    if ( !m_PL->getTableMat( "covarTable", mat_X ) )
    {
        if ( m_logFILE ) *m_logFILE << "Error: HEIDI reading covariates" << endl;
        abort();
    }
    *m_logFILE << "#covar: " << mat_X.n_cols << endl;

    /******************************************************/


    mat_Kb.zeros( m_numInds, m_numInds );
    mat_Kr.zeros( m_numInds, m_numInds );

        

    // reading all SNPs at once may require too much memory
    // read them in batches to fit into 1Gb 1073741824 = ( numInds * BATCH * 16 )
    int BATCH = (maxMemoryGb*67108864) / m_numInds;
    BATCH = 256*(BATCH / 256);
   
    int repeat;
    unsigned long top;
    unsigned long bottom;

    if ( !isRRMwrite && RRMgwFilename.size() )
    {
        if ( m_logFILE ) *m_logFILE << "HEIDI: Loading RRM for genome" << endl;
        mat_Kb.load( RRMgwFilename, raw_ascii);
    }
    else
    {
        if ( m_logFILE ) *m_logFILE << "HEIDI: Computing RRM for genome" << endl;
        if ( m_logFILE ) *m_logFILE << "HEIDI: SNP batch size: " << BATCH << endl;

        repeat = m_numSNPs / BATCH;
        repeat += m_numSNPs % BATCH > 0 ? 1 : 0;
        for ( int i=0; i < repeat; i++ )
        {
            top = (i+1)*BATCH < m_numSNPs ? (i+1)*BATCH : m_numSNPs;
            bottom = i*BATCH;
            m_PL->getSNPMat( bottom+1, top+1, mat_allSNPs );
            mat_Kb += mat_allSNPs * trans( mat_allSNPs );
        }
        mat_Kb = mat_Kb / double( m_numSNPs );

        if ( isRRMwrite )
        {
            mat_Kb.save( RRMgwFilename, raw_ascii );
        }
    }


    m_numRegionSNPs = regionStopSNPIndex - regionStartSNPIndex;
    m_numBackgroundSNPs = m_numSNPs - m_numRegionSNPs;

    if ( !useWholeGenome && (m_numRegionSNPs == 0) )
    {
        if ( m_logFILE ) *m_logFILE << "HEIDI: Region contains no SNPs!" << endl;
        cerr << "Read the log file!" << endl;
        abort();
    }

    if ( !useWholeGenome && !isRRMwrite && RRMrFilename.size() )
    {
        if ( m_logFILE ) *m_logFILE << "HEIDI: Loading RRM for region" << endl;
        mat_Kr.load( RRMrFilename, raw_ascii );
    }
    else if ( !useWholeGenome )
    {
        if ( m_logFILE ) *m_logFILE << "HEIDI: Computing RRM for region" << endl;
        if ( m_logFILE ) *m_logFILE << "HEIDI: SNP batch size: " << BATCH << endl;
        if ( m_logFILE ) *m_logFILE << "HEIDI: #SNPs in region: " << m_numRegionSNPs << endl;
        
        if ( BATCH > m_numRegionSNPs )
        {
            repeat = 1;
        }
        else
        {
            repeat = m_numRegionSNPs / BATCH;
            repeat += m_numRegionSNPs % BATCH > 0 ? 1 : 0;
        }
        for ( int i=0; i < repeat; i++ )
        {
            bottom = regionStartSNPIndex + i*BATCH;
            top = bottom + (i+1)*BATCH;
            if ( top > regionStopSNPIndex )
            {
                top = regionStopSNPIndex;
            }
            m_PL->getSNPMat( bottom, top, mat_regionSNPs );
            mat_Kr += mat_regionSNPs * trans( mat_regionSNPs );
        }
    
        mat_Kr /= double( m_numRegionSNPs );
        
        if ( isRRMwrite )
        {
            mat_Kr.save( RRMrFilename, raw_ascii );
        }
        
        mat_Kb = double(m_numSNPs)/double(m_numBackgroundSNPs)*mat_Kb - double(m_numRegionSNPs)/double(m_numBackgroundSNPs)*mat_Kr;
        m_wTop = 10.0*double( m_numRegionSNPs) / double( m_numSNPs );
    }

    
    double tmp = double( m_numInds - mat_X.n_cols );
    double val, sign;
    log_det( val, sign, trans(mat_X) * mat_X );
    CONSTANT = tmp*( 1.0 + log(2.0 * datum::pi) - log(tmp) ) - val;
}

HEIDI::~HEIDI()
{

    m_logFILE = NULL;
    m_PL = NULL;
}

void HEIDI::setw( double w )
{

    *m_logFILE << "w:= " << w << endl;
    m_w = w;
    K = w*mat_Kr + (1.0-w)*mat_Kb;
    eig_sym(evals, H, K, "dc");
    
    // Magic touch
    for ( int i=0; i < m_numInds; i++ )
    {
        if ( evals(i) < 1e-5 )
        {
            evals(i) = 1e-5;
        }
    }

    y = trans( H ) * mat_y;
    X = trans( H ) * mat_X;
}

void HEIDI::seth2( double h2 )
{
    if ( abs( h2 - m_h2 ) > 1e-10 )
    {
        m_h2 = h2;
        setP( m_h2 );
    }
}


void HEIDI::setV( double h2 )
{
    // if ( m_logFILE ) *m_logFILE << "V( " << h2 << " )" << endl;
    vec tmp = h2*evals + (1.0-h2);
    V = diagmat( tmp );
    Vinv = diagmat( pow( tmp , -1 ) );
}

void HEIDI::setP( double h2 )
{
    //if ( m_logFILE ) *m_logFILE << "P( " << h2 << " )" << endl;
    setV( h2 );
    mat TMP = trans(X) * Vinv;
    XtVinvX = TMP * X;
    mat t;
    if ( !solve( t, XtVinvX, TMP ) )
    {
        t = pinv( XtVinvX ) * TMP;
    }
    P = Vinv - trans(TMP) * t;

    return;
}



double HEIDI::getREML( double h2 )
{
    seth2( h2 );
    double val, sign;
    log_det( val, sign, XtVinvX );
    
    double n_p = double( m_numInds - X.n_cols );

    double ret = CONSTANT + sum( log( diagvec( V ) ) ) + val + n_p*log( dot( trans(y) ,  P * y ) );
    return -0.5*ret;
}

double HEIDI::getdREML( double h2 )
{
    seth2( h2 );
    
    double n_p = double( m_numInds - X.n_cols );
    
    vec Py = P * y;
    mat TMP = diagmat( evals - 1.0 );
    double ret = trace( P * TMP ) - n_p * dot( trans(Py),  TMP * Py ) / dot( trans(y), Py );
    return -0.5*ret;
}


void HEIDI::testREML()
{
    setw( 0.1 );
    double h2;
    cout << "h2\tREML\tdREML" << endl;
    cout.precision(10);
    for ( int i=0; i<500; i++ )
    {
        h2 = double(i)*0.002;
        cout << h2 << "\t" << getREML( h2 ) << "\t" << getdREML(h2) << endl;
    }
}



double HEIDI::BrentsMethodSolve( MemFn function, double lowerLimit, double upperLimit, double errorTol)
{
    double a = lowerLimit;
    double b = upperLimit;
    double c = 0;
    double d = 1e5;

    double fa = (this->*function)(a);
    double fb = (this->*function)(b);

    double fc = 0;
    double s = 0;
    double fs = 0;

    // if f(a) f(b) >= 0 then error-exit
    if (fa * fb >= 0)
    {
        if (fa < fb)
            return a;
        else
            return b;
    }

    // if |f(a)| < |f(b)| then swap (a,b) end if
    if (abs(fa) < abs(fb))
    { double tmp = a; a = b; b = tmp; tmp = fa; fa = fb; fb = tmp; }

    c = a;
    fc = fa;
    bool mflag = true;
    int i = 0;

    while (!(fb==0) && ( abs(a-b) > errorTol))
    {
        if ((fa != fc) && (fb != fc))
            // Inverse quadratic interpolation
            s = a * fb * fc / (fa - fb) / (fa - fc) + b * fa * fc / (fb - fa) / (fb - fc) + c * fa * fb / (fc - fa) / (fc - fb);
        else
            // Secant Rule
            s = b - fb * (b - a) / (fb - fa);

        double tmp2 = (3.0 * a + b) / 4.0;
        if ((!(((s > tmp2) && (s < b)) || ((s < tmp2) && (s > b)))) || (mflag && ( abs(s - b) >= ( abs(b - c) / 2.0))) || (!mflag && ( abs(s - b) >= ( abs(c - d) / 2.0))))
        {
            s = (a + b) / 2.0;
            mflag = true;
        }
        else
        {
            if ((mflag && ( abs(b - c) < errorTol)) || (!mflag && ( abs(c - d) < errorTol)))
            {
                s = (a + b) / 2.0;
                mflag = true;
            }
            else
                mflag = false;
        }
        fs = (this->*function)(s);
        d = c;
        c = b;
        fc = fb;
        if (fa * fs < 0) { b = s; fb = fs; }
        else { a = s; fa = fs; }

        // if |f(a)| < |f(b)| then swap (a,b) end if
        if ( abs(fa) < abs(fb))
        { 
            double tmp = a; a = b; b = tmp; tmp = fa; fa = fb; fb = tmp; 
        }
        i++;
        if (i > 1000)
        {
            break;
        }
    }
    return b;
}



double HEIDI::GoldenSectionSolve( MemFn function, double lowerLimit, double upperLimit, double errorTol)
{
    double a = lowerLimit;
    double b = upperLimit;
    double ratio = 0.61803398875;
    double x1 = b - ratio * (b - a);
    double x2 = a + ratio * (b - a);
    double f1 = (this->*function)(x1);
    double f2 = (this->*function)(x2);

    int i = 0;
    while( abs(b - a) > errorTol )
    {
        if ( f2 < f1 )
        {
            b = x2;
            x2 = x1;
            f2 = f1;
            x1 = b - ratio * (b - a);
            f1 = (this->*function)(x1);
        }
        else
        {
            a = x1;
            x1 = x2;
            f1 = f2;
            x2 = a + ratio * (b - a);
            f2 = (this->*function)(x2);
        }
        i++;
        if ( i > m_numGoldenSteps )
        {
            break;
        }
    }

    return 0.5*(a+b);
}
