#include "../include/readPLINK.h"

readPLINKBED::readPLINKBED( string bedPrefix, ofstream* logFILE )
{
    m_bedPrefix = bedPrefix;
    
    m_numByteIndividuals = 0;
    m_numIndividuals = 0;
    m_numSNPs = 0;
    
    m_logFILE = logFILE;
    
    if ( !m_logFILE->is_open() )
    {
        cerr << "Error: bad logFILE" << endl;
        abort();
    }

    sqlite3_initialize();
    m_DB = NULL;
    char* errorMessage;

    m_bedFilename = bedPrefix + ".bed";
    m_famFilename = bedPrefix + ".fam";
    m_bimFilename = bedPrefix + ".bim";

    m_numIndividuals = 0;
    m_numSNPs = 0;


    // input streams
    ifstream bedFILE( m_bedFilename.c_str(), ios::binary );
    if ( !bedFILE.is_open() )
    {
        *m_logFILE << "Error: Cannot open BED file for reading" << endl;
        abort();
    }
    bedFILE.close();

    if ( !checkBEDFile() )
    {
        cerr << "This BED format is not yet..." << endl;
        abort();
    }
    else
    {
        *m_logFILE << "BED file is in v1.00 SNP-major format" << endl;
    }

    // FILES EXIST SO CREATE SQLITE DATABASE
    *m_logFILE << "CREATING DATABASE TABLES" << endl;
    if ( sqlite3_open( ":memory:", &m_DB ) != SQLITE_OK )
    {
        *m_logFILE << "Error: Cannot open database" << endl;
        sqlite3_close( m_DB );
        abort();
    }

    if ( !createTableBIM() )
    {
        *m_logFILE << "Error creating BIM TABLE" << endl;
        abort();
    }

    if ( !createTableFAM() )
    {
        *m_logFILE << "Error creating FAM TABLE" << endl;
        abort();
    }


    *m_logFILE << "#Individuals: " << m_numIndividuals << endl;
    *m_logFILE << "#SNPs: " << m_numSNPs << endl;


    /*
    // TEST QUERY
    char buffer[100] = "SELECT * FROM bimTable WHERE chromosome='1'";
    sqlite3_stmt* stmt;
    sqlite3_prepare_v2(m_DB, buffer, -1, &stmt, NULL);
    int retval;
    while(1)
    {
        retval = sqlite3_step( stmt );
        if(retval == SQLITE_ROW)
        {
            
            for ( int col=0; col < 7; col++ )
            {
                cout << (const char *)sqlite3_column_text( stmt, col ) << " "; 
            }
            
            //cout << sqlite3_column_int( stmt, 0 ) << " " << sqlite3_column_int( stmt, 4 );
            //cout << endl;
        }
        else
        {
            break;
        }
    }
    */

    // use this to read from BED
    div_t d = div( m_numIndividuals, 4 );
    m_numByteIndividuals = (d.rem > 0) ? d.quot+1 : d.quot;

}



readPLINKBED::~readPLINKBED()
{
    // CLOSE DATABASE
    if ( m_DB )
    {
        sqlite3_close( m_DB );
        m_DB = NULL;
    }
    sqlite3_shutdown();
}


bool readPLINKBED::checkBEDFile()
{
    ifstream bedFILE( m_bedFilename.c_str(), ios::binary );
    //check bed v1.00 format
    bitset<8> magic1( string( "01101100" ) );
    bitset<8> magic2( string( "00011011" ) );

    char buf1, buf2;
    char mode;

    bedFILE.read(&buf1,1);
    bedFILE.read(&buf2,1);
    bedFILE.read(&mode,1);

    bitset<8> a( buf1 );
    bitset<8> b( buf2 );

    bedFILE.close();
    
    return  ( (a^magic1) == 0x00 ) && ( (b^magic2) == 0x00 ) && ( mode == 1);

}

void readPLINKBED::getSNPVec( char* buffer, vec& snpVec )
{
    int numMissing = 0;
    int isMissing[1000] = {0};

    double n0=0.0, n1=0.0, n2=0.0;

    int i=0;
    for ( int b=0; b<m_numByteIndividuals; b++ )
    {
        char row = buffer[b];
        for ( int ind=0; ind<4 && i<m_numIndividuals; ind++)
        {
            char g = row & 0x03;
            switch (g)
            {
                case 0:
                    snpVec[i] = 2.0;
                    n2 += 1.0;
                    break;
                case 1:
                    snpVec[i] = 666.0; // MISSING
                    isMissing[ numMissing ] = i;
                    numMissing++;
                    break;
                case 2:
                    snpVec[i] = 1.0;
                    n1 += 1.0;
                    break;
                case 3:
                    snpVec[i] = 0.0;
                    n0 += 1.0;
                    break;
            }
            i++;
            row >>= 2;
        }
    }

    double n = (double)( m_numIndividuals - numMissing);
    double minor = ( n0 < n2 ) ? n0 : n2;
    double maf = ( 2.0*minor + n1 ) / n;

    if ( n0 < n2 )
    {
        for ( i=0; i < m_numIndividuals; i++ )
        {
            if ( snpVec[i] == 0.0 )
            {
                snpVec[i] = 2.0;
            }
            else if ( snpVec[i] == 2.0 )
            {
                snpVec[i] = 0.0;
            }
        }
    }

    if ( numMissing )
    {
        for ( i=0; i<numMissing; i++ )
        {
            snpVec[ isMissing[i] ] = maf;
        }
    }

    double sd = stddev( snpVec );
    
    for ( i=0; i < m_numIndividuals; i++ )
    {
        snpVec[ i ] = (snpVec[i] - maf) / sd;
    }
}



void readPLINKBED::printSNP(unsigned long index )
{
    // return the normalized SNP vector
    // skip first 3 bytes.
    ifstream bedFILE( m_bedFilename.c_str(), ios::binary );
    int jump = 3+(index-1)*m_numByteIndividuals;
    bedFILE.seekg( jump, ios::beg);

    char *buffer = new char[ m_numByteIndividuals ];
    bedFILE.read( buffer, m_numByteIndividuals );
    
    vec snpVec( m_numIndividuals );
    
    getSNPVec( buffer, snpVec );


    for (int i=0; i<m_numIndividuals; i++ )
        cout << snpVec[i] << ", ";
    cout << endl;
    

    bedFILE.close();
    delete [] buffer;
}


void readPLINKBED::getSNPMat( unsigned long startIndex, unsigned long stopIndex, mat& snpMat )
{
    ifstream bedFILE( m_bedFilename.c_str(), ios::binary );
    int jump = 3+(startIndex-1)*m_numByteIndividuals;
    bedFILE.seekg( jump, ios::beg);
    int N = stopIndex - startIndex;
    char *buffer = new char[ N*m_numByteIndividuals ];
    
    bedFILE.read( buffer, N*m_numByteIndividuals  );

    snpMat.reshape( m_numIndividuals, N );
    
    vec snpVec( m_numIndividuals );
    for ( int col=0; col<N; col++ )
    {
        getSNPVec( &buffer[ col*m_numByteIndividuals ], snpVec );
        snpMat.col( col ) = snpVec;
    }
    delete [] buffer;
}



void readPLINKBED::printSNPs( unsigned long startIndex, unsigned long stopIndex )
{
    int N = stopIndex - startIndex;
    mat W( m_numIndividuals, N );
    getSNPMat( startIndex, stopIndex, W );
    cout << W << endl;
}

int readPLINKBED::createTableBIM()
{
    
    char *errorMessage; 
    ifstream bimFILE( m_bimFilename.c_str() );
    
    if ( !bimFILE.is_open() )
    {
        *m_logFILE << "Error: Cannot open PLINK files for reading" << endl;
        return 0;
    }
    
    
    // Parsing
    boost::char_separator<char> sep(" \t");
    typedef boost::tokenizer< boost::char_separator<char> > Tokenizer;
    //

    char createBIMTable[] =  "CREATE TABLE bimTable (id INTEGER PRIMARY KEY AUTOINCREMENT, chromosome TEXT, rsid TEXT, gendist INTEGER, position INTEGER, allele1 TEXT, allele2 TEXT);";

    if ( sqlite3_exec( m_DB, createBIMTable, NULL, NULL, &errorMessage ) != SQLITE_OK )
    {
        *m_logFILE << "SQL Error: " << string(errorMessage) << endl;
        return 0;
    }

    *m_logFILE << "POPULATING bimTable TABLE FROM: " <<  m_bimFilename << endl;
    // FOR SPEED WE INSERT IN A SINGLE TRANSACTION
    if ( sqlite3_exec(m_DB, "BEGIN TRANSACTION", NULL, NULL, &errorMessage) != SQLITE_OK )
    {
        *m_logFILE << "SQL Error: " << string(errorMessage) << endl;
        return 0;
    }
    
    string line;
    char insertStmt[256];
    vector<string> vec;
    
    while( getline( bimFILE, line) )
    {
        m_numSNPs++;
        Tokenizer tok( line, sep );
        vec.assign( tok.begin(), tok.end() );
        sprintf( insertStmt, "INSERT INTO bimTable VALUES ( NULL, '%s', '%s', %s, %s, '%s', '%s')", vec[0].c_str(), vec[1].c_str(), vec[2].c_str(), vec[3].c_str(), vec[4].c_str(), vec[5].c_str() );
        if ( sqlite3_exec( m_DB, insertStmt, NULL, NULL, &errorMessage ) != SQLITE_OK )
        {
            *m_logFILE << "SQL Error: " << string(errorMessage) << endl;
            return 0;
        }
    }
    bimFILE.close();
    if ( sqlite3_exec(m_DB, "COMMIT TRANSACTION", NULL, NULL, &errorMessage) != SQLITE_OK )
    {
        *m_logFILE << "SQL Error: " << string(errorMessage) << endl;
        return 0;
    }
    return 1;
}


int readPLINKBED::createTableFAM()
{
    char *errorMessage;
    ifstream famFILE( m_famFilename.c_str() );
    
    if ( !famFILE.is_open() )
    {
        *m_logFILE << "Error: Cannot open PLINK files for reading" << endl;
        return 0;
    }

    // Parsing
    boost::char_separator<char> sep(" \t");
    typedef boost::tokenizer< boost::char_separator<char> > Tokenizer;
    //

    char createFAMTable[] =  "CREATE TABLE famTable (familyID TEXT, individualID TEXT PRIMARY KEY, paternalID TEXT, maternalID TEXT, sex INTEGER, affection INTEGER);";
    
    if ( sqlite3_exec( m_DB, createFAMTable, NULL, NULL, &errorMessage ) != SQLITE_OK )
    {
        *m_logFILE << "SQL Error: " << string(errorMessage) << endl;
        return 0;
    }


    *m_logFILE << "POPULATING famTable TABLE FROM: " <<  m_famFilename << endl;
    // FOR SPEED WE INSERT IN A SINGLE TRANSACTION
    sqlite3_exec(m_DB, "BEGIN TRANSACTION", NULL, NULL, NULL);
    
    string line;
    char insertStmt[256];
    vector<string> vec;

    while( getline( famFILE, line) )
    {
        m_numIndividuals++;
        Tokenizer tok( line, sep );
        vec.assign( tok.begin(), tok.end() );
        
        sprintf( insertStmt, "INSERT INTO famTable VALUES ( '%s', '%s', '%s', '%s', %s, %s)", vec[0].c_str(), vec[1].c_str(), vec[2].c_str(), vec[3].c_str(), vec[4].c_str(), vec[5].c_str() );
        if ( sqlite3_exec( m_DB, insertStmt, NULL, NULL, &errorMessage ) != SQLITE_OK )
        {
            *m_logFILE << "SQL Error: " << string(errorMessage) << endl;
            return 0;
        }
    }
    famFILE.close();
    if ( sqlite3_exec(m_DB, "COMMIT TRANSACTION", NULL, NULL, &errorMessage) != SQLITE_OK )
    {
        *m_logFILE << "Error: " << string(errorMessage) << endl;
        return 0;
    }

    return 1;
}


int readPLINKBED::createTableGENERIC( string tableName, string filename  )
{
    // This table reads genetic individual data
    // First two columns are familyID, individualID
    // checks the number of columns to reads and populates

    char *errorMessage;
    ifstream fpFILE( filename.c_str() );
    
    if ( !fpFILE.is_open() )
    {
        *m_logFILE << "Error: Cannot open file: " << filename << " for reading" << endl;
        return 0;
    }

    // Parsing
    boost::char_separator<char> sep(" \t");
    typedef boost::tokenizer< boost::char_separator<char> > Tokenizer;
    //

    string line;
    char insertStmt[256];
    vector<string> vec;

    // Get the number of columns:
    getline( fpFILE, line );
    Tokenizer tok( line, sep );
    vec.assign( tok.begin(), tok.end() );
    int numCol = vec.size();

    fpFILE.seekg(0, ios::beg);

    if ( numCol == 2 )
    {
        *m_logFILE << "Error: There are only 2 columns in " << filename << endl;
        return 0;
    }
    
    *m_logFILE << "POPULATING " << tableName << " TABLE FROM: " <<  filename << endl;


    string createTable = "CREATE TABLE " + tableName + "(familyID TEXT, individualID TEXT PRIMARY KEY";
    for ( int i=2; i < numCol; i++ )
    {
        createTable += ", covar" + to_string(i-1) + " REAL ";
    }
    createTable += ");";

    if ( sqlite3_exec( m_DB, createTable.c_str(), NULL, NULL, &errorMessage ) != SQLITE_OK )
    {
        *m_logFILE << "SQL Error: " << string(errorMessage) << endl;
        return 0;
    }
    
    // NOW INSERT INTO TABLE
    sqlite3_stmt *stmt = NULL;
    string command = "INSERT INTO " + tableName + " VALUES  ( ?1";
    for ( int i = 1; i < numCol; i++ )
    {
        command += ", ?" + to_string( i+1 );
    }
    command += " );";

    if ( sqlite3_prepare_v2( m_DB, command.c_str(), command.size()+1, &stmt, NULL ) != SQLITE_OK )
    {
        *m_logFILE << "SQL Error in preparing " << command << endl;
        return 0;
    }
    
    while( getline( fpFILE, line) )
    {
        Tokenizer tok( line, sep );
        vec.assign( tok.begin(), tok.end() );
    
        sqlite3_bind_text( stmt, 1, vec[0].c_str(), vec[0].size(), SQLITE_STATIC );
        sqlite3_bind_text( stmt, 2, vec[1].c_str(), vec[1].size(), SQLITE_STATIC );
        
        for ( int i=2; i < numCol; i++ )
        {
           sqlite3_bind_double( stmt, i+1, stod( vec[i] ) );
        }

        if ( sqlite3_step( stmt ) == SQLITE_ERROR )
        {
            *m_logFILE << "SQL Error inserting into: " << tableName << endl;
            return 0;
        }
        sqlite3_reset( stmt );
        sqlite3_clear_bindings( stmt );
    }

    sqlite3_finalize( stmt );
    stmt = NULL;

    return 1;
}



int readPLINKBED::printTableGENERIC( string tableName ) const
{
    string command = "SELECT * FROM " + tableName;
    sqlite3_stmt *stmt = NULL;

    if ( sqlite3_prepare_v2( m_DB, command.c_str(), command.size()+1, &stmt, NULL )  != SQLITE_OK )
    {
        *m_logFILE << "SQL Error in prepare " << command << endl;
        return 0;
    } 
    
    int numCol = sqlite3_column_count(stmt);
    for ( int i=0; i < numCol; i++)
    {
        cout << sqlite3_column_name(stmt, i) << " ";
    }
    cout << endl;


    while( sqlite3_step( stmt ) == SQLITE_ROW )
    {
        for ( int col=0; col < numCol; col++ )
        {
            cout << (const char *)sqlite3_column_text( stmt, col ) << " "; 
        }
        cout << endl;
    }
    cout << endl;
    sqlite3_finalize(stmt);
    
    return 1;
}


int readPLINKBED::getTableMat( string tableName, mat& M ) const
{

    string command = "SELECT * FROM " + tableName;
    sqlite3_stmt *stmt = NULL;

    if ( sqlite3_prepare_v2( m_DB, command.c_str(), command.size()+1, &stmt, NULL )  != SQLITE_OK )
    {
        *m_logFILE << "SQL Error in prepare " << command << endl;
        return 0;
    } 
    
    int numCol = sqlite3_column_count(stmt);

    M.resize( m_numIndividuals, numCol-2 );
    
    int row=-1;
    while( sqlite3_step( stmt ) == SQLITE_ROW )
    {
        row++;
        for ( int col=2; col < numCol; col++ )
        {
            M(row, col-2) = sqlite3_column_double( stmt, col ); 
        }
    }
    sqlite3_finalize(stmt);
    
    return 1;
}


int readPLINKBED::getChromSNPIndices( string chromosome, unsigned long& snpStartIndex, unsigned long& snpStopIndex ) const
{
    sqlite3_stmt *stmt = NULL;
    string command = "SELECT id FROM bimTable WHERE chromosome = ?1 ORDER BY position ASC";

    if ( sqlite3_prepare_v2( m_DB, command.c_str(), command.size()+1, &stmt, NULL ) != SQLITE_OK )
    {
        *m_logFILE << "SQL Error in preparing " << command << endl;
        return 0;
    }

    sqlite3_bind_text( stmt, 1, chromosome.c_str(), chromosome.size(), SQLITE_STATIC );
    
    
    if ( sqlite3_step( stmt ) == SQLITE_ROW )
    {
        snpStartIndex = sqlite3_column_int64( stmt, 0 );
    }
    else
    {
        sqlite3_finalize(stmt);
        *m_logFILE << "SQL Error cannot find chromosome: " << chromosome << endl;
        abort();
  
    }
   
    while( sqlite3_step( stmt ) == SQLITE_ROW )
    {
        snpStopIndex = sqlite3_column_int64( stmt, 0 );
    }
    
    sqlite3_finalize(stmt);
    return 1;
}



int readPLINKBED::getSNPIndex( string chromosome, string snpID, unsigned long& snpIndex ) const
{
    sqlite3_stmt *stmt = NULL;
    string command = "SELECT id FROM bimTable WHERE chromosome = ?1 AND rsid = ?2 ORDER BY position ASC LIMIT 1";

    if ( sqlite3_prepare_v2( m_DB, command.c_str(), command.size()+1, &stmt, NULL ) != SQLITE_OK )
    {
        *m_logFILE << "SQL Error in preparing " << command << endl;
        return 0;
    }

    sqlite3_bind_text( stmt, 1, chromosome.c_str(), chromosome.size(), SQLITE_STATIC );
    sqlite3_bind_text( stmt, 2, snpID.c_str(), snpID.size(), SQLITE_STATIC );
    
    int ret = sqlite3_step( stmt );
    if ( ret == SQLITE_ROW )
    {
        snpIndex = sqlite3_column_int64( stmt, 0 );
        sqlite3_finalize( stmt );
        return 1;
    }
    else
    {
        sqlite3_finalize(stmt);
        *m_logFILE << "SQL Error cannot find SNP: " << snpID << " in chromosome: " << chromosome << endl;
        abort();
    }
}

int readPLINKBED::getSNPIndex( string chromosome, unsigned long positionBP, unsigned long& snpIndex ) const
{

    sqlite3_stmt *stmt = NULL;
    string command = "SELECT id FROM bimTable WHERE chromosome = ?1 AND position > ?2 ORDER BY position ASC LIMIT 1";

    if ( sqlite3_prepare_v2( m_DB, command.c_str(), command.size()+1, &stmt, NULL ) != SQLITE_OK )
    {
        *m_logFILE << "SQL Error in preparing " << command << endl;
        return 0;
    }

    sqlite3_bind_text( stmt, 1, chromosome.c_str(), chromosome.size(), SQLITE_STATIC );
    sqlite3_bind_int64( stmt, 2, positionBP );
    
    int ret = sqlite3_step( stmt );
    if ( ret == SQLITE_ROW )
    {
        snpIndex = sqlite3_column_int64( stmt, 0 );
        sqlite3_finalize(stmt);
        return 1;
    }
    else
    {
        *m_logFILE << "SQL Error cannot find a SNP at position (bp): " << positionBP << endl;
        sqlite3_finalize(stmt);
        abort();
    }
}

