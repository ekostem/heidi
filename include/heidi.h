#ifndef HEIDI_H
#define HEIDI_H

#include "readPLINK.h"

#include <iostream>
#include "armadillo"

using namespace std;
using namespace arma;


class readPLINKBED;

class HEIDI
{
public:
    typedef double (HEIDI::*MemFn)(double);

    HEIDI( string chrom, string fromSNP, string toSNP, readPLINKBED* PL, ofstream* logFILE, int maxMemoryGb=2, string RRMgwFilename="", string RRMrFilename="", bool isRRMwrite=false );
    HEIDI( string chrom, double fromKB, double toKB, readPLINKBED* PL, ofstream* logFILE, int maxMemoryGb=2, string RRMgwFilename="", string RRMrFilename="", bool isRRMwrite=false  );
    HEIDI( string chrom, readPLINKBED* PL, ofstream* logFILE, int maxMemoryGb=2, string RRMgwFilename="", string RRMrFilename="", bool isRRMWrite=false  ); 
    

    void Init( unsigned long regionStartSNPIndex, unsigned long regionStopSNPIndex, int maxMemoryGb, string RRMgwFilename="", string RRMrFilename="", bool isRRMwrite=false, bool wholeGenome=false  );
    ~HEIDI();

    void usePhenNo(int i){ mat_y = mat_Y.col(i); } // choose which phenotype to use
    
    void setw( double w );
    
    double getw() { return m_w; } 
    double getwTop() { return m_wTop;}

    void setNumGoldenSteps( int n ){ m_numGoldenSteps = n; }

    void seth2( double h2 );
    double geth2() { return m_h2; }

    double getREML( double h2 );
    double getdREML( double h2 );
    double maxREML_h2(){ return BrentsMethodSolve( &HEIDI::getdREML, 0, 1.0 ); }
    
    double getREML_w( double w ){ setw( w ); seth2(0.0); return getREML( maxREML_h2() ); }
    double maxREML_w(){ return GoldenSectionSolve(&HEIDI::getREML_w, 0, m_wTop); }
    
    void testREML();


private:
    double CONSTANT;
    mat mat_Y, mat_y, mat_X, mat_allSNPs, mat_regionSNPs; // phenotypes, covariates, all SNPs, region SNPs
    mat mat_Kr, mat_Kb; // region & background kinships
    sp_mat mat_I; // m_numInds x m_numInds identity matrix


    /*** These are internal matrices for a particular w **/
    mat K; // w*Kr + (1-w)Kb
    mat H; // eigen vector
    mat X, P;
    vec y;
    mat V, Vinv, XtVinvX;
    vec evals;
    void setV( double h2 );
    void setP( double h2 );
    /*****************************************************/

    double m_Vg, m_Ve, m_w, m_h2; // w \in [0,1]
    double m_wTop;

    int m_numPheno, m_numInds;
    long m_numSNPs;
    long m_numRegionSNPs;
    long m_numBackgroundSNPs;

    readPLINKBED *m_PL;
    ofstream* m_logFILE;

    /********** Brent's Method ************************/
    double BrentsMethodSolve( MemFn function, double lowerLimit, double upperLimit, double errorTol=1e-5);
    /***************************************************/

    /*****************Golden Section Search***************/
    double GoldenSectionSolve( MemFn function, double lowerLimit, double upperLimit, double errorTol=1e-5);
    int m_numGoldenSteps;
    /*******************************************************/

};





#endif
