#ifndef READPLINK_H
#define READPLINK_H

#include <iostream>
#include <fstream>
#include <string>
#include <tuple>
#include <vector>
#include <bitset>


#include <algorithm>
#include <iterator>

#include <boost/tokenizer.hpp>
#include "sqlite3.h"

#include "armadillo"

using namespace std;
using namespace arma;

class readPLINKBED
{
   
public:
    readPLINKBED( string bedPrefix, ofstream* logFILE );
    ~readPLINKBED();
    
    int getNumIndividuals( void ){return m_numIndividuals;} 
    int getNumSNPs( void ){ return m_numSNPs;} 


    void getSNPMat( unsigned long startIndex, unsigned long stopIndex, mat& snpMat );
    int getTableMat( string tableName, mat& M ) const;

    void printSNP( unsigned long index );
    void printSNPs( unsigned long startIndex, unsigned long stopIndex );

    int createTableGENERIC( string tableName, string filename  );
    int printTableGENERIC( string tableName ) const;
    
    int getChromSNPIndices( string chromosome, unsigned long& snpStartIndex, unsigned long& snpStopIndex ) const;
    int getSNPIndex( string chromosome, string snpID, unsigned long& snpIndex ) const;
    int getSNPIndex( string chromosome, unsigned long positionBP, unsigned long& snpIndex ) const;

private:
    string m_bedPrefix;
    string m_bedFilename, m_famFilename, m_bimFilename;
    unsigned long m_numSNPs;
    int m_numIndividuals;
    int m_numByteIndividuals;
    sqlite3* m_DB;
    ofstream* m_logFILE;

    // FUNCTIONS
    bool checkBEDFile();

    int createTableBIM();
    int createTableFAM();
    
    void getSNPVec( char* buffer, vec& snpVec ); 
};




#endif

